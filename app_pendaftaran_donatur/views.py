from django.shortcuts import render, redirect
from .forms import FormUser
from .models import User

# Create your views here.
def pendaftaran(request):
    form = FormUser(request.POST or None)
    response = {}
    users = User.objects.all()
    response["form"] = form
    response["users"] = users
    if(request.method == "POST" and form.is_valid()):
        nama = request.POST.get("nama")
        tanggal = request.POST.get("tanggal")
        email = request.POST.get("email")
        password1 = request.POST.get("password1")
        password2 = request.POST.get("password2")
        user = User(nama=nama, tanggal=tanggal, email=email, password1=password1, password2=password2)
        user.full_clean()
        user.save()
        return redirect('programs:list_program')
    else:
        return render(request, "pendaftaran_donatur.html", response)