from django import forms
from .models import User
from django.core.validators import RegexValidator

class FormUser(forms.Form):
    nama = forms.CharField(label="Nama")
    tanggal = forms.DateField(widget=forms.DateInput(attrs={'type' : 'date'}), label="Tanggal Lahir")
    email = forms.EmailField(label="Email")
    password1 = forms.CharField(label='Password', required=True, min_length=8, max_length=12, validators=[RegexValidator('^(\w+\d+|\d+\w+)+$', message="Kata sandi harus mengandung kombinasi alfabet dan angka")], widget=forms.PasswordInput())
    password2 = forms.CharField(label='Confirm Password', required=True, min_length=8, max_length=12, widget=forms.PasswordInput())

    def clean(self):
        cleaned_data = super().clean()
        emails = User.objects.all()
        email = cleaned_data.get("email")

        for i in emails:
            if i.email == email:
                raise forms.ValidationError(
                "Email sudah pernah digunakan"
            )
    
    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Kata sandi tidak cocok")