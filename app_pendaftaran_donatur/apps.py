from django.apps import AppConfig


class AppPendaftaranDonaturConfig(AppConfig):
    name = 'app_pendaftaran_donatur'
