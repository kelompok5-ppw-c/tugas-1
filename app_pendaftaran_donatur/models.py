from django.db import models

# Create your models here.
class User(models.Model):
    nama = models.CharField(max_length=100)
    tanggal = models.DateField()
    email = models.EmailField(max_length=50, unique=True)
    password1 = models.CharField(max_length=12)
    password2 = models.CharField(max_length=12)