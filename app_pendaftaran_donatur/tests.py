from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.forms import *
from .views import pendaftaran
from .models import User
from .forms import FormUser
import unittest

# Create your tests here.
class Pendaftaran_Donatur_Test(TestCase):
    def test_pendaftaran_donatur_url_is_exist(self):
        response = Client().get('/pendaftaran_donatur/')
        self.assertEqual(response.status_code, 200)

    def test_pendaftaran_donatur_using_to_do_list_template(self):
        response = Client().get('/pendaftaran_donatur/')
        self.assertTemplateUsed(response, 'pendaftaran_donatur.html')

    def test_pendaftaran_donatur_using_pendaftaran_func(self):
        found= resolve('/pendaftaran_donatur/')
        self.assertEqual(found.func, pendaftaran)

    def test_model_can_create_new_user(self):
        new_user = User.objects.create(nama="me", tanggal="1999-07-12", email="itsme@yahoo.com", password1="hello1234", password2="hello1234")
        counting_all_user = User.objects.all().count()
        self.assertEqual(counting_all_user, 1)

    def test_UserForm_valid(self):
        form = FormUser(data={'nama': "me",'tanggal': "1999-07-12", 'email': "user@yahoo.com" , 'password1': "user1234", 'password2': "user1234"})
        self.assertTrue(form.is_valid())

    def test_UserForm_invalid(self):
        new_user = User.objects.create(nama="me", tanggal="1999-07-12", email="itsme@yahoo.com", password1="hello1234", password2="hello1234")
        form = FormUser(data={'nama': "me",'tanggal': "1999-07-12", 'email': "itsme@yahoo.com" , 'password1': "user1234", 'password2': "user1234"})
        self.assertFalse(form.is_valid())