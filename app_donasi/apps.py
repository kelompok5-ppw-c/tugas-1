from django.apps import AppConfig


class AppDonasiConfig(AppConfig):
    name = 'app_donasi'
