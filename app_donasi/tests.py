from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Program
from .views import list_program
import tempfile
import unittest

# Create your tests here.
class Donasi_Test(TestCase):
    def test_donasi_url_is_exist(self):
        response = Client().get('/donasi/')
        self.assertEqual(response.status_code, 200)

    def test_donasi_using_to_do_list_template(self):
        response = Client().get('/donasi/')
        self.assertTemplateUsed(response, 'donasi.html')

    def test_donasi_using_pendaftaran_func(self):
        found= resolve('/donasi/')
        self.assertEqual(found.func, list_program)

    # def test_not_logged_in(self):
    #     request = HttpRequest()
    #     response = index(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('Login terlebih dahulu', html_response)
    
    # def test_user_logged_in(self):
    #     self.credentials = {
    #         'username'
    #     }

    def test_model_can_create_new_program(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        new_program = Program.objects.create(status="lorem ipsum", 
        name = "Bencana",
        photo = image,
        desc ="lorem ipsum dolor sit amet",
        target = 2000000,
        remDay = 24)
        counting_all_programs = Program.objects.all().count()
        self.assertEqual(counting_all_programs, 1)