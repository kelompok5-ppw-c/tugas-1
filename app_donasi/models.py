from django.db import models
from app_formulir_donasi.models import Donasi

# Create your models here.
class Program(models.Model):
    status = models.CharField(max_length=100)
    name   = models.CharField(max_length=200)
    photo  = models.ImageField(upload_to="static/img")
    desc   = models.TextField()
    target = models.IntegerField()
    remDay = models.IntegerField()
    donatur = models.ManyToManyField(Donasi)
