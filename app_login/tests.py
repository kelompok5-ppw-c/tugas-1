from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import login


# Create your tests here.
class User_Auth_Test(TestCase):
    def test_login_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    # def test_sign_up_url_is_exist(self):
    #     response = Client().get('/accounts/signup/')
    #     self.assertEqual(response.status_code, 200)

    def test_used_login_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    # def test_used_sign_up_template(self):
        # response = Client().get('/login/profil/')
        # self.assertTemplateUsed(response, 'profil.html')

    def test_using_create_form_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login)
