from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from app_formulir_donasi.models import Donasi

# Create your views here.
def login(request):
    if request.user.is_authenticated:
        return redirect('profil')
    return render(request, "login.html")

def profil(request):
	if request.user.is_authenticated:
		if 'nama' not in request.session:
			request.session['nama'] = request.user.first_name + " " + request.user.last_name
		if 'username' not in request.session:
			request.session['username'] = request.user.username
		if 'email' not in request.session:
			request.session['email'] = request.user.email
		email2 = request.user.email
		# print('email2')
		donasi_filter = Donasi.objects.filter(email=email2)
		totalUang = 0
		for donasiX in donasi_filter:
			totalUang+= donasiX.jmldonasi
		response = {
			'donasi_filter' : donasi_filter,
			'totalUang' : totalUang,
			'email2' : email2,
		}
		return render(request, "profil.html", response)    
	return redirect('login')

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))