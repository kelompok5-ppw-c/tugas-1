from django.apps import AppConfig


class AppFormulirDonasiConfig(AppConfig):
    name = 'app_formulir_donasi'
