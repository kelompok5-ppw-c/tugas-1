from django import forms
from app_pendaftaran_donatur.models import User

class FormDonasi(forms.Form):
    # nama = forms.CharField(label="Nama", required=False)
    # email = forms.EmailField(label="Email")
    jmldonasi = forms.FloatField(label="Jumlah Donasi", required=True, min_value=5000)
    is_anon = forms.BooleanField(widget=forms.CheckboxInput, required=False, label="Donasi sebagai anonim")

    # def clean(self):
    #     cleaned_data = super().clean()
    #     nama = cleaned_data.get("nama")
    #     is_anon = cleaned_data.get("is_anon")

    #     if not is_anon and not nama:
    #         raise forms.ValidationError(
    #             "Nama kosong"
    #         )

    # def clean(self):
    #     cleaned_data = super().clean()
    #     emails = User.objects.all()
    #     email = cleaned_data.get("email")
        
    #     for i in emails:
    #         if i.email != email:
    #             raise forms.ValidationError(
    #             "Email belum pernah terdaftar, lakukan pendaftaran terlebih dahulu!"
    #         )
    #         return email