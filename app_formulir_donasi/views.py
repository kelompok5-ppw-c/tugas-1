from django.shortcuts import render, redirect
from .forms import FormDonasi
from .models import Donasi
from app_pendaftaran_donatur.models import User
from app_donasi.models import Program
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def formulir(request, program):
    form = FormDonasi(request.POST or None)
    nama_program = Program.objects.get(pk=program).name

    program2 = Program.objects.get(pk=program)
    print(program2.name)
    donasi2 = Donasi.objects.all()
    response = {
        'nama_program' : nama_program,
        'donasi2': donasi2,
        'program2': program2
        }
    response['nama'] = request.session['nama']
    response['email'] = request.session['email']
    response["form"] = form
    if(request.method == "POST" and form.is_valid()):
        nama = response['nama']
        email = response['email']
        jmldonasi = request.POST.get("jmldonasi")
        is_anon = request.POST.get("is_anon")
        if is_anon:
            program2.donatur.add(Donasi.objects.create(nama="anonim", email=email, jmldonasi=jmldonasi, is_anon=True, namaProgram=program2.name))
        else:
            program2.donatur.add(Donasi.objects.create(nama=nama, email=email, jmldonasi=jmldonasi, is_anon=False, namaProgram=program2.name))
        return redirect('donasi_berhasil')
    else:
        return render(request, 'formulir_donasi.html', response)

def berhasil(request):
    response={}
    return render(request,'donasi_berhasil.html',response)