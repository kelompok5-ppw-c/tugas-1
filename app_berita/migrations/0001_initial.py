# Generated by Django 2.1.1 on 2018-10-17 17:34

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Berita',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('judul', models.CharField(max_length=200)),
                ('gambar', models.ImageField(upload_to='static/img')),
                ('isi', models.TextField()),
            ],
        ),
    ]
