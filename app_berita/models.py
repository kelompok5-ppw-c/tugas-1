from django.db import models

# Create your models here.
class Berita(models.Model):
    sumber = models.CharField(max_length=100)
    judul = models.CharField(max_length=200)
    gambar = models.ImageField(upload_to="static/img")
    tanggal_publish = models.CharField(max_length=50)
    isi = models.TextField()
