from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Berita
from .views import berita
import tempfile
import unittest

# Create your tests here.
class Berita_Test(TestCase):
    def test_berita_url_is_exist(self):
        response = Client().get('/berita/')
        self.assertEqual(response.status_code, 200)

    def test_berita_using_to_do_list_template(self):
        response = Client().get('/berita/')
        self.assertTemplateUsed(response, 'berita.html')

    def test_berita_using_pendaftaran_func(self):
        found= resolve('/berita/')
        self.assertEqual(found.func, berita)

    def test_model_can_create_new_news(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        new_news = Berita.objects.create(sumber="LIPUTAN 6", 
        judul = "Selama Dibutuhkan, AS Siap Terus Bantu Sulawesi Tengah Bangkit dari Bencana",
        gambar = image,
        tanggal_publish = "2018-10-11T07:31:00Z",
        isi ="lorem ipsum dolor sit amet")
        counting_all_news = Berita.objects.all().count()
        self.assertEqual(counting_all_news, 1)