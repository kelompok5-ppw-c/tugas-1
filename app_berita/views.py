from django.shortcuts import render
from .models import Berita

# Create your views here.
def berita(request):
    beritas = Berita.objects.all()
    response = {}
    response['beritas'] = beritas
    return render(request,'berita.html',response)
