from django.apps import AppConfig


class CrowdFundingConfig(AppConfig):
    name = 'crowd_funding'
