from django.shortcuts import render, redirect

# Create your views here.
def home(request):
    response = {}
    return render(request,'home.html',response)