from django.test import TestCase

from django.test import Client
from django.urls import resolve
from .views import home

# Create your tests here.

class Crowd_fundingUnitTest(TestCase):

    def test_crowd_funding_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_berita_using_to_do_list_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_crowd_funding_using_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)