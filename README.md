```bash
Kelompok    : 5
Kelas       : PPW-C
Anggota     : Aulia Rosyida (1706025346)
              M Abdurrahman Hermanto (1706039925)
              Oktafia Sutanti (1706023486)
              Roshani Ayu Pranasti (1706026052)
```

## Link Heroku
CROWD FUNDING: SinarPerak<br>
http://ppw-c-kel5.herokuapp.com/

## Status Aplikasi
[![Pipeline](https://gitlab.com/kelompok5-ppw-c/tugas-1/badges/master/pipeline.svg)](https://gitlab.com/kelompok5-ppw-c/tugas-1/commits/master)
[![Coverage](https://gitlab.com/kelompok5-ppw-c/tugas-1/badges/master/coverage.svg)](https://gitlab.com/kelompok5-ppw-c/tugas-1/commits/master)

## Pembagian Tugas
Tugas Pemrograman 1:
1. Aulia Rosyida: Program-program Donasi
2. M Abdurrahman Hermanto: Berita
3. Oktafia Sutanti: Pendaftaran Donasi
4. Roshani Ayu Pranasti: Pendaftaran Donatur dan Tampilan Web

Tugas Pemrograman 2:
1. Aulia Rosyida: Daftar Donasi yang pernah dilakukan (bagi yang sudah login)
2. M Abdurrahman Hermanto:<br> 
+Tombol donasi kalau sudah login diklik langsung donate, kalau belum login diklik minta login<br>
+Donasi ke suatu program (bagi yang sudah login)<br>
3. Oktafia Sutanti:<br>
+Halaman About - plus menampilkan komentar yang sudah tersimpan (tanpa login)<br>
+Testimoni untuk halaman about - kalau tidak login tidak muncul text area testimoni
4. Roshani Ayu Pranasti: Login dengan Google dan Tampilan Web