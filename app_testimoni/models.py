from django.db import models

# Create your models here.
class Testimoni(models.Model):
	nama = models.CharField(max_length=200, blank=True)
	testimoni = models.CharField(max_length=500)
	created_at = models.DateTimeField(auto_now_add=True)