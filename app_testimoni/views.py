from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse
from .forms import TestimoniForm
from .models import Testimoni
import requests, json, re

# Create your views here.
def testimoni(request):
	response_data = {}
	testimonis = Testimoni.objects.all()
	form_value = TestimoniForm()
	return render(request, "testimoni.html", {'form':form_value, 'testimonis':testimonis})

@csrf_exempt
def post(request):
	response_data = {}
	if (request.method == "POST"):
		post_text = request.POST.get('the_post')
		response_data['nama'] = request.session['nama']
		nama = response_data['nama']
		newObj = Testimoni.objects.create(nama=nama, testimoni=post_text)
		print(newObj.created_at)
		time = newObj.created_at.strftime("%b. %d, %Y, %I:%M %p")
		response_data['message'] = post_text
		response_data['time'] = time
		# except :	
			# response_data['message'] = "tidak masuk"
		return HttpResponse(json.dumps(response_data), content_type="application/json")
	else:
		return HttpResponse(json.dumps({"nothing to see": "this isn't happening"}), content_type="application/json")
