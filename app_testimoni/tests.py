# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve
from .views import testimoni, post
from .models import Testimoni
from .forms import TestimoniForm

class Lab10UnitTest(TestCase):
	def test_testimoni_url_is_exist(self):
		response = Client().get('/testimoni/')
		self.assertEqual(response.status_code,200)
		
	def test_using_template(self):
		response = Client().get('/testimoni/')
		self.assertTemplateUsed(response, 'testimoni.html')
		
	def test_using_testimoni_func(self):
		found = resolve('/testimoni/')
		self.assertEqual(found.func, testimoni)
		
	def test_model_can_create_new_testimoni(self):
        # Creating a new activity
		new_testimoni = Testimoni.objects.create(testimoni = 'hallo!')

        # Retrieving all available activity
		counting_all_available_todo = Testimoni.objects.all().count()
		self.assertEqual(counting_all_available_todo, 1)
		
	def test_using_create_post_func(self):
		found = resolve('/testimoni/post/')
		self.assertEqual(found.func, post)
	
	# def test_can_save_a_POST_request(self):
		# response = self.client.post('/testimoni/post/', data = {
			# 'message':'save testi', 
		# })
		# counting_all_available_testimonial = Testimoni.objects.all().count()
		# self.assertEqual(counting_all_available_testimonial, 1)
		
		# self.assertEqual(response.status_code, 302)
		# self.assertEqual(response['location'], '/testimoni/post/')
		
		# new_response = self.client.get('/testimoni/post/')
		# html_response = new_response.content.decode('utf8')
		# self.assertIn('', html_response)


	
