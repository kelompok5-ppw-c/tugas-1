from django import forms
from .models import Testimoni

# implement forms using ModelForm
class TestimoniForm(forms.ModelForm):
    class Meta:
        model = Testimoni
        fields = ['testimoni']
        widgets = {
            'testimoni': forms.Textarea(
                attrs={'class': 'form-control', 'id': 'post-text',  'placeholder': 'Tulis testimoni anda disini..' , 'maxlength': 500}),
        }

